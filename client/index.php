<?php

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

require 'vendor/autoload.php';

function connect(): AMQPStreamConnection
{
    try {
        return new AMQPStreamConnection(getenv('RABBITMQ_HOST'), getenv('RABBITMQ_PORT'), 'guest', 'guest');
    } catch (Exception $exception) {
        $stdout = fopen('php://stderr', 'w');
        fputs($stdout, $exception->getMessage() . PHP_EOL);
        sleep(60);
        return connect();
    }
}

$connection = connect();

$channel = $connection->channel();
$channel->queue_declare('fms-validator', false, false, false, false);
$data = [
    // invalid
    3601630821,
    3601630823,
    1107539582,
    1107539584,
    1107539590,
    1107539591,
    1107539625,
    1107539633,
    4508981049,
    4508989087,
    // valid
    6601630821,
    6601630823,
    6107539582,
    6107539584,
    6107539590,
    6107539591,
    6107539625,
    6107539633,
    6508981049,
    6508989087,
];

while (true) {
    $msg = new AMQPMessage($data[rand(0, 19)]);
    $channel->basic_publish($msg, '', 'fms-validator');
    $notify = " [x] " . $msg->getBody();
    $stdout = fopen('php://stdout', 'w');
    fputs($stdout, $notify . PHP_EOL);
    sleep(1);
}

$channel->close();
$connection->close();