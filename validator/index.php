<?php

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

require 'vendor/autoload.php';

function connect(): AMQPStreamConnection
{
    try {
        return new AMQPStreamConnection(getenv('RABBITMQ_HOST'), getenv('RABBITMQ_PORT'), 'guest', 'guest');
    } catch (Exception $exception) {
        $stdout = fopen('php://stderr', 'w');
        fputs($stdout, $exception->getMessage() . PHP_EOL);
        sleep(60);
        return connect();
    }
}

$connection = connect();

$channel = $connection->channel();
$channel->queue_declare('fms-validator', false, false, false, false);
$redis = new Redis();

$redis->pconnect(
    getenv('REDIS_HOST'),
    getenv('REDIS_PORT')
);

$handler = new \App\Handler($redis);

$channel->basic_consume('fms-validator', '', false, true, false, false, $handler);
while(count($channel->callbacks)) {
    $channel->wait();
}

$redis->close();
$channel->close();
$connection->close();