<?php

namespace App;

use PhpAmqpLib\Message\AMQPMessage;

class Handler
{
    private $stdout;
    /**
     * @var \Redis
     */
    private $redis;

    public function __construct(\Redis $redis)
    {
        $this->stdout = fopen('php://stdout', 'w');
        $this->redis = $redis;
    }

    public function __invoke(AMQPMessage $message)
    {
        $this->stdout(' [x] Received ' . $message->body);
        $start = microtime(true);

        if ($this->redis->exists($message->body)) {
            $this->stdout('invalid');
            // TODO: отправка сообщения AMQP о недействительном паспорте
        } else {
            $this->stdout('valid');
        }

        $delta = microtime(true) - $start;
        $this->stdout($delta * 1000 . ' ms');
    }

    /**
     * @param string $message
     */
    private function stdout(string $message): void
    {
        fputs($this->stdout, $message . PHP_EOL);
    }
}